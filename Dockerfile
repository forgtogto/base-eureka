FROM openjdk:11
EXPOSE 8761
ARG JAR_FILE=target/*.jar
ENV TZ=Asia/Seoul
COPY ${JAR_FILE} ROOT.jar
ENTRYPOINT ["java","-jar","/ROOT.jar"]