# EUREKA Server for JKSEO

### 1. Create JAR

```
mvn install
```

### 2. Build Dockerfile 

```
sudo docker build -t jkseo-eureka/spring-boot .
```

### 2. Run Dockerfile 

```
sudo docker run -d -p 8761:8761 --name jkseo-eureka-server jkseo-eureka/spring-boot
```
